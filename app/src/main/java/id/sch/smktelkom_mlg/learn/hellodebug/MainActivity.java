package id.sch.smktelkom_mlg.learn.hellodebug;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {

    TextView tvHello;
    TextView tvDua;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        tvDua = findViewById(R.Id.textViewDua);
        tvHello.setText("Ini Hello " + tvDua.getText());
    }
}
